import pygame
from EventManager import *
from Models import GameEngine
from Constants.Screen import Screen
from Models.Button import Button
from Controllers.Initializers.Initializations import Initializations

class ViewController(object):
    """
    Draws the various states onto the screen.
    """
    #initializations = Initializations

    def __init__(self, event_manager, game_engine, initializations):
        """
        evManager (EventManager): Allows posting messages to the event queue.
        model (GameEngine): a strong reference to the game Model.
                
        Attributes:
        isinitialized (bool): pygame is ready to draw.
        screen (pygame.Surface): the screen surface.
        clock (pygame.time.Clock): keeps the fps constant.
        smallfont (pygame.Font): a small font.
        """
        
        self.event_manager = event_manager
        event_manager.RegisterListener(self)
        self.game_engine = game_engine
        self.isinitialized = False
        self.screen = None
        self.clock = None
        self.small_font = None
        self.initializations = initializations
        self.sound_on_button = self.initializations.make_sound_on_button()
        self.sound_off_button = self.initializations.make_sound_off_button()
        self.cat_sound_on_button = self.initializations.make_cat_sound_on_button()
        self.cat_sound_off_button = self.initializations.make_cat_sound_off_button()

    
    def notify(self, event):
        """
        Receive events posted to the message queue. This set of if statements check the current state
        and then initialize the iinitializations.object list with the necessary objects to display the
        proper screen

        Inside the if statements below for each screen: the first if the sound is already playing and if it should
        be before initializing the mixer with the proper tune. The second if statement switches the sound on/off
        graphic to the "on" one and the next if statement switches the sound on/off graphic to the "off" one. Finally
        it renders the view with the porper elements stored in the proper dictionary list
        """

        if isinstance(event, InitializeEvent):
            self.initialize()
        elif isinstance(event, QuitEvent):
            # shut down the pygame graphics
            self.isinitialized = False
            pygame.quit()
        elif isinstance(event, TickEvent):
            if not self.isinitialized:
                return
            current_state = self.game_engine.state.peek()
            busy = pygame.mixer.music.get_busy()
            if current_state == GameEngine.STATE_CATEGORIES:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['categories'][0] = self.cat_sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['categories'][0] = self.cat_sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['categories'])
            elif current_state == GameEngine.STATE_ADULT:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['adult'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['adult'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['adult'])
            elif current_state == GameEngine.STATE_ADULT2:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['adult2'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['adult2'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['adult2'])
            elif current_state == GameEngine.STATE_PARTY:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['party'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['party'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['party'])
            elif current_state == GameEngine.STATE_PARTY2:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['party2'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['party2'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['party2'])
            elif current_state == GameEngine.STATE_KIDS:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['kids'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['kids'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['kids'])
            elif current_state == GameEngine.STATE_KIDS2:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['kids2'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['kids2'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['kids2'])
            elif current_state == GameEngine.STATE_CHILL:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['chill'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['chill'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['chill'])
            elif current_state == GameEngine.STATE_CHILL2:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/category/category.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['chill2'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['chill2'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['chill2'])
            elif current_state == GameEngine.STATE_GAME_APPLES_TO_APPLES:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['apples_to_apples'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['apples_to_apples'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['apples_to_apples'])
            elif current_state == GameEngine.STATE_GAME_BATTLESHIP:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['battleship'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['battleship'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['battleship'])
            elif current_state == GameEngine.STATE_GAME_CANDY_LAND:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['candy_land'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['candy_land'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['candy_land'])
            elif current_state == GameEngine.STATE_GAME_CARDS_AG_HUM:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['cards_ag_hum'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['cards_ag_hum'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['cards_ag_hum'])
            elif current_state == GameEngine.STATE_GAME_CHECKERS:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['checkers'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['checkers'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['checkers'])
            elif current_state == GameEngine.STATE_GAME_CHESS:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['chess'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['chess'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['chess'])
            elif current_state == GameEngine.STATE_GAME_CHINESE_CHECKERS:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['chinese_checkers'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['chinese_checkers'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['chinese_checkers'])
            elif current_state == GameEngine.STATE_GAME_GENERIC:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['generic'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['generic'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['generic'])
            elif current_state == GameEngine.STATE_GAME_HANGMAN:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['hangman'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['hangman'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['hangman'])
            elif current_state == GameEngine.STATE_GAME_HEADS_UP:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['heads_up'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['heads_up'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['heads_up'])
            elif current_state == GameEngine.STATE_GAME_JENGA:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['jenga'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['jenga'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['jenga'])
            elif current_state == GameEngine.STATE_GAME_LIFE:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['life'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['life'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['life'])
            elif current_state == GameEngine.STATE_GAME_MAFIA:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['mafia'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['mafia'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['mafia'])
            elif current_state == GameEngine.STATE_GAME_MAHJONG:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['mahjong'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['mahjong'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['mahjong'])
            elif current_state == GameEngine.STATE_GAME_MANCALA:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['mancala'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['mancala'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['mancala'])
            elif current_state == GameEngine.STATE_GAME_MONOPOLY:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['monopoly'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['monopoly'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['monopoly'])
            elif current_state == GameEngine.STATE_GAME_POKER:
                busy = pygame.mixer.music.get_busy()
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['poker'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['poker'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['poker'])
            elif current_state == GameEngine.STATE_GAME_PONG:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['pong'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['pong'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['pong'])
            elif current_state == GameEngine.STATE_GAME_RISK:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['risk'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['risk'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['risk'])
            elif current_state == GameEngine.STATE_GAME_SCRABBLE:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['scrabble'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['scrabble'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['scrabble'])
            elif current_state == GameEngine.STATE_GAME_SETTLERS_KATAN:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['settlers_katan'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['settlers_katan'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['settlers_katan'])
            elif current_state == GameEngine.STATE_GAME_SNAKES_LADDERS:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['snakes_ladders'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['snakes_ladders'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['snakes_ladders'])
            elif current_state == GameEngine.STATE_GAME_SORRY:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['sorry'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['sorry'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['sorry'])
            elif current_state == GameEngine.STATE_GAME_TABOO:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['taboo'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['taboo'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['taboo'])
            elif current_state == GameEngine.STATE_GAME_WORDS_WITH_FRIENDS:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['words_with_friends'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['words_with_friends'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['words_with_friends'])
            elif current_state == GameEngine.STATE_GAME_YAHTZEE:
                if not busy and self.initializations.sound_play == True:
                    pygame.mixer.init()
                    pygame.mixer.music.load('assets/adult/generic.mp3')
                    pygame.mixer.music.play(-1)
                if self.initializations.sound_changed == True and self.initializations.sound_play == True:
                    self.initializations.objects['yahtzee'][1] = self.sound_on_button
                    self.initializations.sound_changed = False
                elif self.initializations.sound_changed == True and self.initializations.sound_play == False:
                    self.initializations.objects['yahtzee'][1] = self.sound_off_button
                    self.initializations.state_changed = False
                self.render_view(self.initializations.objects['yahtzee'])
            elif current_state == GameEngine.STATE_HELP:
                self.render_help()
            # limit the redraw speed to 30 frames per second
            self.clock.tick(Screen.MAX_FPS)

#this function actually draws the screen
    def render_view(self, new_object_list):
        """
        Render the categories menu.
        """

        self.screen.fill((87, 73, 73))
        for object in new_object_list:
            object.draw()

        pygame.display.flip()

#this function renders a help screen if we decide to develop one later
    def render_help(self):
        """
        Render the help screen.
        """

        self.screen.fill((87, 73, 73))
        some_words = self.small_font.render(
                    'Help is here. space, escape or return.', 
                    True, (0, 255, 0))
        self.screen.blit(some_words, (0, 0))
        pygame.display.flip()

#this function initializes all the graphics
    def initialize(self):
        """
        Set up the pygame graphical display and loads graphical resources.
        """

        result = pygame.init()
        pygame.font.init()
        pygame.display.set_caption('DBGInterface')
        self.screen = pygame.display.set_mode((Screen.SCREEN_WIDTH, Screen.SCREEN_HEIGHT), pygame.FULLSCREEN)
        self.clock = pygame.time.Clock()
        self.small_font = pygame.font.Font(None, 40)
        self.isinitialized = True
        pygame.mixer.init()

