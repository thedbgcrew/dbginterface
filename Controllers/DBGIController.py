import pygame
from EventManager import *
from Models import GameEngine
import os
import subprocess
from Controllers.Initializers.Initializations import Initializations

class DBGIController(object):
    """
    Handles keyboard and mouse input.
    """

    #initializations = Initializations

    def __init__(self, event_manager, game_engine, initializations):
        """
        evManager (EventManager): Allows posting messages to the event queue.
        model (GameEngine): a strong reference to the game Model.
        """
        self.event_manager = event_manager
        event_manager.RegisterListener(self)
        self.game_engine = game_engine
        self.initializations = initializations
        self.time = pygame.time.get_ticks()

    def notify(self, event):
        """
        Receive events posted to the message queue. This set of if statements check the current state
        and then initialize the iinitializations.object list with the necessary objects to control the
        states
        """

        if isinstance(event, TickEvent):
            # Called for each game tick. We check our keyboard presses here.
            for event in pygame.event.get():
                currentstate = self.game_engine.state.peek()
                # handle window manager closing our window
                if event.type == pygame.QUIT:
                    self.event_manager.Post(QuitEvent())
                # handle key down events
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.event_manager.Post(StateChangeEvent(None))
                        pygame.mixer.music.stop()
                    else:
                        if currentstate == GameEngine.STATE_CATEGORIES:
                            self.key_down_categories(event)
                        elif currentstate == GameEngine.STATE_ADULT:
                            self.key_down_game_picker(event)
                        elif currentstate == GameEngine.STATE_HELP:
                            self.key_down_help(event)
                elif event.type == pygame.MOUSEBUTTONUP:
                    if currentstate == GameEngine.STATE_CATEGORIES:
                        self.mouse_click_categories(event)
                    elif currentstate == GameEngine.STATE_ADULT:
                        self.mouse_click_game_picker_adult(event)
                    elif currentstate == GameEngine.STATE_ADULT2:
                        self.mouse_click_game_picker_adult2(event)
                    elif currentstate == GameEngine.STATE_PARTY:
                        self.mouse_click_game_picker_party(event)
                    elif currentstate == GameEngine.STATE_PARTY2:
                        self.mouse_click_game_picker_party2(event)
                    elif currentstate == GameEngine.STATE_KIDS:
                        self.mouse_click_game_picker_kids(event)
                    elif currentstate == GameEngine.STATE_KIDS2:
                        self.mouse_click_game_picker_kids2(event)
                    elif currentstate == GameEngine.STATE_CHILL:
                        self.mouse_click_game_picker_chill(event)
                    elif currentstate == GameEngine.STATE_CHILL2:
                        self.mouse_click_game_picker_chill2(event)
                    elif currentstate == GameEngine.STATE_GAME_APPLES_TO_APPLES:
                        self.mouse_click_game_apples_to_apples(event)
                    elif currentstate == GameEngine.STATE_GAME_BATTLESHIP:
                        self.mouse_click_game_battleship(event)
                    elif currentstate == GameEngine.STATE_GAME_CANDY_LAND:
                        self.mouse_click_game_candy_land(event)
                    elif currentstate == GameEngine.STATE_GAME_CARDS_AG_HUM:
                        self.mouse_click_game_cards_ag_hum(event)
                    elif currentstate == GameEngine.STATE_GAME_CHECKERS:
                        self.mouse_click_game_checkers(event)
                    elif currentstate == GameEngine.STATE_GAME_CHESS:
                        self.mouse_click_game_chess(event)
                    elif currentstate == GameEngine.STATE_GAME_CHINESE_CHECKERS:
                        self.mouse_click_game_chinese_checkers(event)
                    elif currentstate == GameEngine.STATE_GAME_GENERIC:
                        self.mouse_click_game_generic(event)
                    elif currentstate == GameEngine.STATE_GAME_HEADS_UP:
                        self.mouse_click_game_heads_up(event)
                    elif currentstate == GameEngine.STATE_GAME_HANGMAN:
                        self.mouse_click_game_hangman(event)
                    elif currentstate == GameEngine.STATE_GAME_JENGA:
                        self.mouse_click_game_jenga(event)
                    elif currentstate == GameEngine.STATE_GAME_LIFE:
                        self.mouse_click_game_life(event)
                    elif currentstate == GameEngine.STATE_GAME_MAFIA:
                        self.mouse_click_game_mafia(event)
                    elif currentstate == GameEngine.STATE_GAME_MAHJONG:
                        self.mouse_click_game_mahjong(event)
                    elif currentstate == GameEngine.STATE_GAME_MANCALA:
                        self.mouse_click_game_mancala(event)
                    elif currentstate == GameEngine.STATE_GAME_MONOPOLY:
                        self.mouse_click_game_monopoly(event)
                    elif currentstate == GameEngine.STATE_GAME_POKER:
                        self.mouse_click_game_poker(event)
                    elif currentstate == GameEngine.STATE_GAME_PONG:
                        self.mouse_click_game_pong(event)
                    elif currentstate == GameEngine.STATE_GAME_RISK:
                        self.mouse_click_game_risk(event)
                    elif currentstate == GameEngine.STATE_GAME_SCRABBLE:
                        self.mouse_click_game_scrabble(event)
                    elif currentstate == GameEngine.STATE_GAME_SETTLERS_KATAN:
                        self.mouse_click_game_settlers_katan(event)
                    elif currentstate == GameEngine.STATE_GAME_SNAKES_LADDERS:
                        self.mouse_click_game_snakes_ladders(event)
                    elif currentstate == GameEngine.STATE_GAME_SORRY:
                        self.mouse_click_game_sorry(event)
                    elif currentstate == GameEngine.STATE_GAME_TABOO:
                        self.mouse_click_game_taboo(event)
                    elif currentstate == GameEngine.STATE_GAME_WORDS_WITH_FRIENDS:
                        self.mouse_click_game_words_with_friends(event)
                    elif currentstate == GameEngine.STATE_GAME_YAHTZEE:
                        self.mouse_click_game_yahtzee(event)
                    elif currentstate == GameEngine.STATE_HELP:
                        self.key_down_help(event)

#this function handles mouse input while in the category state
    def mouse_click_categories(self, event):

        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #objects = self.initializations.objects['categories']
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['categories'][1].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
        elif (self.initializations.objects['categories'][3].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_ADULT))
            self.initializations.sound_changed = True
            self.initializations.state_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['categories'][4].within_button(x, y)and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_PARTY))
            self.initializations.sound_changed = True
            self.initializations.state_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['categories'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_KIDS))
            self.initializations.sound_changed = True
            self.initializations.state_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['categories'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_CHILL))
            self.initializations.state_changed = True
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['categories'][0].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker adult screen
    def mouse_click_game_picker_adult(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #objects = self.initializations.objects['adult']
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['adult'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_ADULT2))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_POKER))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CARDS_AG_HUM))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SCRABBLE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HEADS_UP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHESS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SETTLERS_KATAN))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][12].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MAHJONG))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker adult 2nd screen
    def mouse_click_game_picker_adult2(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #objects = self.initializations.objects['adult']
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['adult2'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CARDS_AG_HUM))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HEADS_UP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_PONG))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_YAHTZEE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_RISK))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MAFIA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_TABOO))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_APPLES_TO_APPLES))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['adult2'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker party screen

    def mouse_click_game_picker_party(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['party'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        if (self.initializations.objects['party'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_PARTY2))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CARDS_AG_HUM))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HEADS_UP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_PONG))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_YAHTZEE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_RISK))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MAFIA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_TABOO))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][12].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_APPLES_TO_APPLES))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker party 2ND screen

    def mouse_click_game_picker_party2(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['party2'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_LIFE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SNAKES_LADDERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MONOPOLY))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHINESE_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SORRY))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CANDY_LAND))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MANCALA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_BATTLESHIP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['party2'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker kids screen
    def mouse_click_game_picker_kids(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['kids'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        if (self.initializations.objects['kids'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_KIDS2))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_LIFE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SNAKES_LADDERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MONOPOLY))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHINESE_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SORRY))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CANDY_LAND))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MANCALA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][12].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_BATTLESHIP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker kids 2nd screen
    def mouse_click_game_picker_kids2(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['kids2'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_WORDS_WITH_FRIENDS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_JENGA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HANGMAN))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SCRABBLE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHESS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MANCALA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHINESE_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['kids2'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker chill screen
    def mouse_click_game_picker_chill(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['chill'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        if (self.initializations.objects['chill'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_CHILL2))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_WORDS_WITH_FRIENDS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_JENGA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HANGMAN))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SCRABBLE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHESS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MANCALA))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][12].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHINESE_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the game picker chill 2ND screen
    def mouse_click_game_picker_chill2(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = True
        #the following statements check if we have clicked inside a button and then sets the
        #game state appropriately
        if (self.initializations.objects['chill2'][2].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            if not self.initializations.in_cat:
                pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_POKER))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][5].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CARDS_AG_HUM))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][6].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SCRABBLE))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][7].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_HEADS_UP))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][8].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHECKERS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][9].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_CHESS))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][10].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_SETTLERS_KATAN))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][11].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_GAME_MAHJONG))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chill2'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the apples to apples game screen
    def mouse_click_game_apples_to_apples(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['apples_to_apples'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['apples_to_apples'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the battleship game screen
    def mouse_click_game_battleship(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['battleship'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['battleship'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the candy land game screen
    def mouse_click_game_candy_land(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['candy_land'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['candy_land'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the cards against humanity game screen
    def mouse_click_game_cards_ag_hum(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['cards_ag_hum'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['cards_ag_hum'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the checkers game screen
    def mouse_click_game_checkers(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['checkers'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['checkers'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the chess game screen
    def mouse_click_game_chess(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['chess'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chess'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the chinese checkers game screen
    def mouse_click_game_chinese_checkers(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['chinese_checkers'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['chinese_checkers'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on any of the generic game screens
    def mouse_click_game_generic(self, event):
        """
        Handles game picker mouse click events
        """
        x, y = event.pos
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        if (self.initializations.objects['generic'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['generic'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the hangman game screen
    def mouse_click_game_hangman(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['hangman'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['hangman'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the heads up game screen
    def mouse_click_game_heads_up(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['heads_up'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['heads_up'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the jenga game screen
    def mouse_click_game_jenga(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['jenga'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['jenga'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the life game screen
    def mouse_click_game_life(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['life'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['life'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the life game screen
    def mouse_click_game_mafia(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['mafia'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['mafia'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the mahjong game screen
    def mouse_click_game_mahjong(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['mahjong'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['mahjong'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the mancala game screen
    def mouse_click_game_mancala(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['mancala'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['mancala'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the monopoly game screen
    def mouse_click_game_monopoly(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['monopoly'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['monopoly'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the poker game screen
    def mouse_click_game_poker(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['poker'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['poker'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the poker game screen
    def mouse_click_game_pong(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['pong'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        if (self.initializations.objects['pong'][5].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            subprocess.call('C:/dbg repo/games/pong.exe')
        elif (self.initializations.objects['pong'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the risk game screen
    def mouse_click_game_risk(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['risk'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['risk'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the scrabble game screen
    def mouse_click_game_scrabble(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['scrabble'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['scrabble'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the settlers of katan game screen
    def mouse_click_game_settlers_katan(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['settlers_katan'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['settlers_katan'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the snakes and ladders game screen
    def mouse_click_game_snakes_ladders(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['snakes_ladders'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        if (self.initializations.objects['snakes_ladders'][5].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            subprocess.call('C:/dbg repo/games/snakes.exe')
        elif (self.initializations.objects['snakes_ladders'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the sorry game screen
    def mouse_click_game_sorry(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['sorry'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['sorry'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the taboo game screen
    def mouse_click_game_taboo(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['taboo'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['taboo'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the words with friends game screen
    def mouse_click_game_words_with_friends(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['words_with_friends'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['words_with_friends'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles all mouse input while on the yahtzee game screen
    def mouse_click_game_yahtzee(self, event):
        #gets the mouse position
        x, y = event.pos
        #establishes a time flag so that the program doesn't register multiple mouse clicks
        newtime = pygame.time.get_ticks()
        timediff = newtime - self.time
        self.initializations.in_cat = False
        #the following statements check if we have clicked inside a button and then sets the
        #state appropriately
        if (self.initializations.objects['yahtzee'][4].within_button(x, y) and timediff > 500):
            self.event_manager.Post(StateChangeEvent(None))
            self.initializations.sound_changed = True
            pygame.mixer.music.stop()
        elif (self.initializations.objects['yahtzee'][1].within_button(x, y) and timediff > 500):
            if (self.initializations.sound_play == True):
                self.initializations.sound_play = False
                pygame.mixer.music.stop()
                self.initializations.sound_changed = True
            else:
                self.initializations.sound_play = True
                self.initializations.sound_changed = True
        #set the time the function was last called for the flag above
        self.time = pygame.time.get_ticks()

#this function handles keyboard input while in the category state
    def key_down_categories(self, event):
        """
        Handles category control events.
        """

        # escape pops the menu
        if event.key == pygame.K_ESCAPE:
            self.event_manager.Post(StateChangeEvent(None))
        # space plays the game
        if event.key == pygame.K_SPACE:
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_ADULT))

#handles keyboard input if we are in the help state
    def key_down_help(self, event):
        """
        Handles help key events.
        """

        # space, enter or escape pops help
        if event.key in [pygame.K_ESCAPE, pygame.K_SPACE, pygame.K_RETURN]:
            self.event_manager.Post(StateChangeEvent(None))

#handles keyboard input when we are in the adult state
    def key_down_game_picker(self, event):
        """
        Handles play key events.
        """

        if event.key == pygame.K_ESCAPE:
            self.event_manager.Post(StateChangeEvent(None))
        # F1 shows the help
        if event.key == pygame.K_F1:
            self.event_manager.Post(StateChangeEvent(GameEngine.STATE_HELP))
        else:
            self.event_manager.Post(InputEvent(event.unicode, None))

