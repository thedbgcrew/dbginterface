from Constants.Screen import Screen
from Models.Button import Button

class Initializations (object):

    """
    This class contains all the various initializations for the screens. Each function
    below first initializes a local list, then creates all the button object needed for
    each screen. It then appends all the objects to the local list and finally returns
    the list which overwrites the object list in the initializations object
    """

    objects = {}
    sound_play = True
    in_cat = True
    sound_changed = True
    state_changed = True

    def __init__(self):

        self.name = 0

#this function fills the dictionary with all the button objects for all the screens
    def fill_object_dict(self):
        self.init_categories()
        self.init_game_picker_adult()
        self.init_game_picker_adult2()
        self.init_game_picker_party()
        self.init_game_picker_party2()
        self.init_game_picker_kids()
        self.init_game_picker_kids2()
        self.init_game_picker_chill()
        self.init_game_picker_chill2()
        self.init_game_apples_to_apples()
        self.init_game_battleship()
        self.init_game_candy_land()
        self.init_game_cards_ag_hum()
        self.init_game_checkers()
        self.init_game_chess()
        self.init_game_chinese_checkers()
        self.init_game_generic()
        self.init_game_hangman()
        self.init_game_heads_up()
        self.init_game_jenga()
        self.init_game_life()
        self.init_game_mafia()
        self.init_game_mahjong()
        self.init_game_mancala()
        self.init_game_monopoly()
        self.init_game_poker()
        self.init_game_pong()
        self.init_game_risk()
        self.init_game_scrabble()
        self.init_game_settlers_katan()
        self.init_game_snakes_ladders()
        self.init_game_sorry()
        self.init_game_taboo()
        self.init_game_words_with_friends()
        self.init_game_yahtzee()

    def make_sound_on_button(self):
        sound_on_button = Button('assets/sound_on.png', 0, 955, 125, 125)
        return sound_on_button

    def make_cat_sound_on_button(self):
        cat_sound_on_button = Button('assets/sound_on.png', 1400, 0, 125, 125)
        return cat_sound_on_button

    def make_sound_off_button(self):
        sound_off_button = Button('assets/sound_off.png', 0, 955, 125, 125)
        return sound_off_button

    def make_cat_sound_off_button(self):
        cat_sound_off_button = Button('assets/sound_off.png', 1400, 0, 125, 125)
        return cat_sound_off_button


#this function initializes the object list with things for the categories screen
    def init_categories(self):
        cat_objects = []
        #cat_bkgd = Button('assets/category/catbackground.bmp', 0, 0, Screen.SCREEN_WIDTH, Screen.SCREEN_HEIGHT)
        adult_button = Button('assets/category/adult.png', Screen.SCREEN_WIDTH*.035, Screen.SCREEN_HEIGHT*.12, Screen.SCREEN_WIDTH*.417, Screen.SCREEN_HEIGHT*.37)
        party_button = Button('assets/category/party.png', Screen.SCREEN_WIDTH*.52, Screen.SCREEN_HEIGHT*.12, Screen.SCREEN_WIDTH*.417, Screen.SCREEN_HEIGHT*.37)
        kids_button = Button('assets/category/kids.png', Screen.SCREEN_WIDTH*.035, Screen.SCREEN_HEIGHT*.56, Screen.SCREEN_WIDTH*.417, Screen.SCREEN_HEIGHT*.37)
        chill_button = Button('assets/category/chill.png', Screen.SCREEN_WIDTH*.52, Screen.SCREEN_HEIGHT*.56, Screen.SCREEN_WIDTH*.417, Screen.SCREEN_HEIGHT*.37)
        title_button = Button('assets/category/category_title.png', 820, 40, 257, 63)
        quit_button = Button('assets/quit.jpg', 108, 52, 114, 52)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        #cat_objects.append(cat_bkgd)
        cat_objects.append(sound_button_on)
        cat_objects.append(quit_button)
        cat_objects.append(title_button)
        cat_objects.append(adult_button)
        cat_objects.append(party_button)
        cat_objects.append(kids_button)
        cat_objects.append(chill_button)
        self.objects['categories'] = cat_objects

#this function initializes the object list with things for the adult category screen
    def init_game_picker_adult(self):
        gp_objects = []
        gp_adult_bkgd = Button('assets/adult/adult_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/adult/adult_title.png', 880, 50, 257, 63)
        forward_button = Button('assets/forward.png', 1778, 37, 122, 85)
        adult_button1 = Button('assets/games/poker/poker.png', 144, 160, 300, 300)
        adult_button2 = Button('assets/games/cards_ag_hum/cards_ag_hum.png', 588, 160, 300, 300)
        adult_button3 = Button('assets/games/scrabble/scrabble.png', 1032, 160, 300, 300)
        adult_button4 = Button('assets/games/heads_up/heads_up.png', 1476, 160, 300, 300)
        adult_button5 = Button('assets/games/checkers/checkers.png', 144, 620, 300, 300)
        adult_button6 = Button('assets/games/chess/chess.png', 588, 620, 300, 300)
        adult_button7 = Button('assets/games/settlers_katan/settlers_katan.png', 1032, 620, 300, 300)
        adult_button8 = Button('assets/games/mahjong/mahjong.png', 1456, 620, 400, 200)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(gp_adult_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(forward_button)
        gp_objects.append(adult_button1)
        gp_objects.append(adult_button2)
        gp_objects.append(adult_button3)
        gp_objects.append(adult_button4)
        gp_objects.append(adult_button5)
        gp_objects.append(adult_button6)
        gp_objects.append(adult_button7)
        gp_objects.append(adult_button8)
        self.objects['adult'] = gp_objects

#this function initializes the object list with things for the adult category 2nd screen
    def init_game_picker_adult2(self):
        gp_objects = []
        gp_adult_bkgd = Button('assets/adult/adult_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/adult/adult_title.png', 880, 50, 257, 63)
        adult_button1 = Button('assets/games/cards_ag_hum/cards_ag_hum.png', 144, 160, 300, 300)
        adult_button2 = Button('assets/games/heads_up/heads_up.png', 588, 160, 300, 300)
        adult_button3 = Button('assets/games/pong/pong.png', 1032, 160, 300, 300)
        adult_button4 = Button('assets/games/yahtzee/yahtzee.png', 1476, 160, 300, 300)
        adult_button5 = Button('assets/games/risk/risk.png', 144, 620, 300, 300)
        adult_button6 = Button('assets/games/mafia/mafia.png', 588, 620, 300, 300)
        adult_button7 = Button('assets/games/taboo/taboo.png', 1032, 620, 300, 300)
        adult_button8 = Button('assets/games/apples_to_apples/apples_to_apples.png', 1456, 620, 400, 200)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(gp_adult_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(adult_button1)
        gp_objects.append(adult_button2)
        gp_objects.append(adult_button3)
        gp_objects.append(adult_button4)
        gp_objects.append(adult_button5)
        gp_objects.append(adult_button6)
        gp_objects.append(adult_button7)
        gp_objects.append(adult_button8)
        self.objects['adult2'] = gp_objects

#this function initializes the object list with things for the party category screen
    def init_game_picker_party(self):
        gp_objects = []
        gp_party_bkgd = Button('assets/party/party_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/party/party_title.png', 880, 50, 257, 63)
        forward_button = Button('assets/forward.png', 1778, 37, 122, 85)
        party_button1 = Button('assets/games/cards_ag_hum/cards_ag_hum.png', 144, 160, 300, 300)
        party_button2 = Button('assets/games/heads_up/heads_up.png', 588, 160, 300, 300)
        party_button3 = Button('assets/games/pong/pong.png', 1032, 160, 300, 300)
        party_button4 = Button('assets/games/yahtzee/yahtzee.png', 1476, 160, 300, 300)
        party_button5 = Button('assets/games/risk/risk.png', 144, 620, 300, 300)
        party_button6 = Button('assets/games/mafia/mafia.png', 588, 620, 300, 300)
        party_button7 = Button('assets/games/taboo/taboo.png', 1032, 620, 300, 300)
        party_button8 = Button('assets/games/apples_to_apples/apples_to_apples.png', 1456, 620, 400, 200)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(gp_party_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(forward_button)
        gp_objects.append(party_button1)
        gp_objects.append(party_button2)
        gp_objects.append(party_button3)
        gp_objects.append(party_button4)
        gp_objects.append(party_button5)
        gp_objects.append(party_button6)
        gp_objects.append(party_button7)
        gp_objects.append(party_button8)
        self.objects['party'] = gp_objects

#this function initializes the object list with things for the party 2nd category screen
    def init_game_picker_party2(self):
        gp_objects = []
        gp_party_bkgd = Button('assets/party/party_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/party/party_title.png', 880, 50, 257, 63)
        party_button1 = Button('assets/games/life/life.png', 144, 160, 300, 300)
        party_button2 = Button('assets/games/snakes_ladders/snakes_ladders.png', 588, 160, 300, 300)
        party_button3 = Button('assets/games/monopoly/monopoly.png', 1032, 160, 300, 300)
        party_button4 = Button('assets/games/chinese_checkers/chinese_checkers.png', 1476, 160, 300, 300)
        party_button5 = Button('assets/games/sorry/sorry.png', 144, 620, 300, 300)
        party_button6 = Button('assets/games/candy_land/candy_land.png', 588, 620, 300, 300)
        party_button7 = Button('assets/games/mancala/mancala.png', 1032, 620, 300, 300)
        party_button8 = Button('assets/games/battleship/battleship.png', 1476, 620, 300, 300)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(gp_party_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(party_button1)
        gp_objects.append(party_button2)
        gp_objects.append(party_button3)
        gp_objects.append(party_button4)
        gp_objects.append(party_button5)
        gp_objects.append(party_button6)
        gp_objects.append(party_button7)
        gp_objects.append(party_button8)
        self.objects['party2'] = gp_objects

#this function initializes the object list with things for the kids category screen
    def init_game_picker_kids(self):
        gp_objects = []
        kids_bkgd = Button('assets/kids/kids_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/kids/kids_title.png', 894, 50, 257, 63)
        forward_button = Button('assets/forward.png', 1778, 37, 122, 85)
        kids_button1 = Button('assets/games/life/life.png', 144, 160, 300, 300)
        kids_button2 = Button('assets/games/snakes_ladders/snakes_ladders.png', 588, 160, 300, 300)
        kids_button3 = Button('assets/games/monopoly/monopoly.png', 1032, 160, 300, 300)
        kids_button4 = Button('assets/games/chinese_checkers/chinese_checkers.png', 1476, 160, 300, 300)
        kids_button5 = Button('assets/games/sorry/sorry.png', 144, 620, 300, 300)
        kids_button6 = Button('assets/games/candy_land/candy_land.png', 588, 620, 300, 300)
        kids_button7 = Button('assets/games/mancala/mancala.png', 1032, 620, 300, 300)
        kids_button8 = Button('assets/games/battleship/battleship.png', 1476, 620, 300, 300)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(kids_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(forward_button)
        gp_objects.append(kids_button1)
        gp_objects.append(kids_button2)
        gp_objects.append(kids_button3)
        gp_objects.append(kids_button4)
        gp_objects.append(kids_button5)
        gp_objects.append(kids_button6)
        gp_objects.append(kids_button7)
        gp_objects.append(kids_button8)
        self.objects['kids'] = gp_objects

#this function initializes the object list with things for the kids 2nd category screen
    def init_game_picker_kids2(self):
        gp_objects = []
        gp_party_bkgd = Button('assets/kids/kids_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/kids/kids_title.png', 880, 50, 257, 63)
        kids_button1 = Button('assets/games/words_with_friends/words_with_friends.png', 144, 160, 300, 300)
        kids_button2 = Button('assets/games/jenga/jenga.png', 588, 160, 300, 300)
        kids_button3 = Button('assets/games/hangman/hangman.png', 1032, 160, 300, 300)
        kids_button4 = Button('assets/games/scrabble/scrabble.png', 1476, 160, 300, 300)
        kids_button5 = Button('assets/games/checkers/checkers.png', 144, 620, 300, 300)
        kids_button6 = Button('assets/games/chess/chess.png', 588, 620, 300, 300)
        kids_button7 = Button('assets/games/mancala/mancala.png', 1032, 620, 300, 300)
        kids_button8 = Button('assets/games/chinese_checkers/chinese_checkers.png', 1476, 620, 300, 300)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(gp_party_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(kids_button1)
        gp_objects.append(kids_button2)
        gp_objects.append(kids_button3)
        gp_objects.append(kids_button4)
        gp_objects.append(kids_button5)
        gp_objects.append(kids_button6)
        gp_objects.append(kids_button7)
        gp_objects.append(kids_button8)
        self.objects['kids2'] = gp_objects

#this function initializes the object list with things for the chill category screen
    def init_game_picker_chill(self):
        gp_objects = []
        chill_bkgd = Button('assets/chill/chill_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/chill/chill_title.png', 880, 50, 257, 63)
        forward_button = Button('assets/forward.png', 1778, 37, 122, 85)
        chill_button1 = Button('assets/games/words_with_friends/words_with_friends.png', 144, 160, 300, 300)
        chill_button2 = Button('assets/games/jenga/jenga.png', 588, 160, 300, 300)
        chill_button3 = Button('assets/games/hangman/hangman.png', 1032, 160, 300, 300)
        chill_button4 = Button('assets/games/scrabble/scrabble.png', 1476, 160, 300, 300)
        chill_button5 = Button('assets/games/checkers/checkers.png', 144, 620, 300, 300)
        chill_button6 = Button('assets/games/chess/chess.png', 588, 620, 300, 300)
        chill_button7 = Button('assets/games/mancala/mancala.png', 1032, 620, 300, 300)
        chill_button8 = Button('assets/games/chinese_checkers/chinese_checkers.png', 1476, 620, 300, 300)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(chill_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(forward_button)
        gp_objects.append(chill_button1)
        gp_objects.append(chill_button2)
        gp_objects.append(chill_button3)
        gp_objects.append(chill_button4)
        gp_objects.append(chill_button5)
        gp_objects.append(chill_button6)
        gp_objects.append(chill_button7)
        gp_objects.append(chill_button8)
        self.objects['chill'] = gp_objects

#this function initializes the object list with things for the chill 2nd category screen
    def init_game_picker_chill2(self):
        gp_objects = []
        chill_bkgd = Button('assets/chill/chill_bkgd.png', 0, 0, 1920, 1080)
        back_button = Button('assets/back.png', 44, 37, 122, 85)
        title_button = Button('assets/chill/chill_title.png', 880, 50, 257, 63)
        chill_button1 = Button('assets/games/poker/poker.png', 144, 160, 300, 300)
        chill_button2 = Button('assets/games/cards_ag_hum/cards_ag_hum.png', 588, 160, 300, 300)
        chill_button3 = Button('assets/games/scrabble/scrabble.png', 1032, 160, 300, 300)
        chill_button4 = Button('assets/games/heads_up/heads_up.png', 1476, 160, 300, 300)
        chill_button5 = Button('assets/games/checkers/checkers.png', 144, 620, 300, 300)
        chill_button6 = Button('assets/games/chess/chess.png', 588, 620, 300, 300)
        chill_button7 = Button('assets/games/settlers_katan/settlers_katan.png', 1032, 620, 300, 300)
        chill_button8 = Button('assets/games/mahjong/mahjong.png', 1456, 620, 400, 200)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        gp_objects.append(chill_bkgd)
        gp_objects.append(sound_button_on)
        gp_objects.append(back_button)
        gp_objects.append(title_button)
        gp_objects.append(chill_button1)
        gp_objects.append(chill_button2)
        gp_objects.append(chill_button3)
        gp_objects.append(chill_button4)
        gp_objects.append(chill_button5)
        gp_objects.append(chill_button6)
        gp_objects.append(chill_button7)
        gp_objects.append(chill_button8)
        self.objects['chill2'] = gp_objects


#this function initializes the object list with things for the apples to apples game screen
    def init_game_apples_to_apples(self):
        game_objects = []
        game_bkgd = Button('assets/games/apples_to_apples/apples_to_apples_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/apples_to_apples/apples_to_apples_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/apples_to_apples/apples_to_apples_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/apples_to_apples/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/apples_to_apples/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/apples_to_apples/apples_to_apples_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['apples_to_apples'] = game_objects

#this function initializes the object list with things for the battleship game screen
    def init_game_battleship(self):
        game_objects = []
        game_bkgd = Button('assets/games/battleship/battleship_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/battleship/battleship_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/battleship/battleship_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/battleship/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/battleship/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/battleship/battleship_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['battleship'] = game_objects

#this function initializes the object list with things for the candy land game screen
    def init_game_candy_land(self):
        game_objects = []
        game_bkgd = Button('assets/games/candy_land/candy_land_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/candy_land/candy_land_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/candy_land/candy_land_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/candy_land/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/candy_land/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/candy_land/candy_land_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['candy_land'] = game_objects

#this function initializes the object list with things for the cards against humanity game screen
    def init_game_cards_ag_hum(self):
        game_objects = []
        game_bkgd = Button('assets/games/cards_ag_hum/cards_ag_hum_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/cards_ag_hum/cards_ag_hum_descrip.png', 132, 160, 979, 87)
        game_button2 = Button('assets/games/cards_ag_hum/cards_ag_hum_game_pic.png', 1320, 160, 500, 500)
        game_button3 = Button('assets/games/cards_ag_hum/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/cards_ag_hum/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/cards_ag_hum/cards_ag_hum_title.png', 542, 30, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['cards_ag_hum'] = game_objects

#this function initializes the object list with things for the checkers game screen
    def init_game_checkers(self):
        game_objects = []
        game_bkgd = Button('assets/games/checkers/checkers_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/checkers/checkers_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/checkers/checkers_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/checkers/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/checkers/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/checkers/checkers_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['checkers'] = game_objects

#this function initializes the object list with things for the chess game screen
    def init_game_chess(self):
        game_objects = []
        game_bkgd = Button('assets/games/chess/chess_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/chess/chess_descrip.png', 160, 194, 979, 87)
        game_button2 = Button('assets/games/chess/chess_game_pic.png', 1315, 160, 500, 500)
        game_button3 = Button('assets/games/chess/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/chess/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/chess/chess_title.png', 840, 60, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['chess'] = game_objects

#this function initializes the object list with things for the chinese checkers game screen
    def init_game_chinese_checkers(self):
        game_objects = []
        game_bkgd = Button('assets/games/chinese_checkers/chinese_checkers_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/chinese_checkers/chinese_checkers_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/chinese_checkers/chinese_checkers_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/chinese_checkers/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/chinese_checkers/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/chinese_checkers/chinese_checkers_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['chinese_checkers'] = game_objects

#this function initializes the object list with things for a generic game screen
#it is used for games that have not been developed yet
    def init_game_generic(self):
        game_objects = []
        game_bkgd = Button('assets/games/generic_game/genericgamebkgd.jpg', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/generic_game/generic_descrip.png', 140, 240, 900, 600)
        game_button2 = Button('assets/games/generic_game/generic_game_pic.jpg', 1180, 240, 600, 600)
        game_button3 = Button('assets/games/generic_game/generic_game_back_button.png', 140, 910, 600, 100)
        game_button4 = Button('assets/games/generic_game/generic_game_play_button.png', 1180, 910, 600, 100)
        game_button5 = Button('assets/games/generic_game/generic_title.png', 710, 70, 500, 100)
        sound_button_on = Button('assets/sound_on.png', 1090, 10, 50, 50)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['generic'] = game_objects

#this function initializes the object list with things for the hangman game screen
    def init_game_hangman(self):
        game_objects = []
        game_bkgd = Button('assets/games/hangman/hangman_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/hangman/hangman_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/hangman/hangman_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/hangman/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/hangman/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/hangman/hangman_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['hangman'] = game_objects

#this function initializes the object list with things for the heads game screen
    def init_game_heads_up(self):
        game_objects = []
        game_bkgd = Button('assets/games/heads_up/heads_up_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/heads_up/heads_up_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/heads_up/heads_up_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/heads_up/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/heads_up/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/heads_up/heads_up_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['heads_up'] = game_objects

#this function initializes the object list with things for the heads game screen
    def init_game_jenga(self):
        game_objects = []
        game_bkgd = Button('assets/games/jenga/jenga_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/jenga/jenga_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/jenga/jenga_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/jenga/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/jenga/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/jenga/jenga_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['jenga'] = game_objects

#this function initializes the object list with things for the life game screen
    def init_game_life(self):
        game_objects = []
        game_bkgd = Button('assets/games/life/life_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/life/life_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/life/life_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/life/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/life/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/life/life_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['life'] = game_objects

#this function initializes the object list with things for the mafia game screen
    def init_game_mafia(self):
        game_objects = []
        game_bkgd = Button('assets/games/mafia/mafia_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/mafia/mafia_descrip.png', 102, 213, 1177, 401)
        game_button2 = Button('assets/games/mafia/mafia_game_pic.png', 1352, 214, 500, 500)
        game_button3 = Button('assets/games/mafia/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/mafia/play.png', 1315, 730, 514, 180)
        game_button5 = Button('assets/games/mafia/mafia_title.png', 870, 73, 193, 48)
        sound_button_on = Button('assets/sound_on.png', 11, 962, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['mafia'] = game_objects

#this function initializes the object list with things for the mahjong game screen
    def init_game_mahjong(self):
        game_objects = []
        game_bkgd = Button('assets/games/mahjong/mahjong_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/mahjong/mahjong_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/mahjong/mahjong_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/mahjong/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/mahjong/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/mahjong/mahjong_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['mahjong'] = game_objects

#this function initializes the object list with things for the mancala game screen
    def init_game_mancala(self):
        game_objects = []
        game_bkgd = Button('assets/games/mancala/mancala_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/mancala/mancala_descrip.png', 126, 192, 979, 87)
        game_button2 = Button('assets/games/mancala/mancala_game_pic.png', 1315, 192, 500, 500)
        game_button3 = Button('assets/games/mancala/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/mancala/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/mancala/mancala_title.png', 760, 36, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['mancala'] = game_objects

#this function initializes the object list with things for the monopoly game screen
    def init_game_monopoly(self):
        game_objects = []
        game_bkgd = Button('assets/games/monopoly/monopoly_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/monopoly/monopoly_descrip.png', 140, 214, 979, 87)
        game_button2 = Button('assets/games/monopoly/monopoly_game_pic.png', 1302, 144, 500, 500)
        game_button3 = Button('assets/games/monopoly/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/monopoly/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/monopoly/monopoly_title.png', 712, 60, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['monopoly'] = game_objects

#this function initializes the object list with things for the poker game screen
    def init_game_poker(self):
        game_objects = []
        game_bkgd = Button('assets/games/poker/poker_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/poker/poker_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/poker/poker_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/poker/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/poker/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/poker/poker_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['poker'] = game_objects

#this function initializes the object list with things for the pong game screen
    def init_game_pong(self):
        game_objects = []
        game_bkgd = Button('assets/games/pong/pong_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/pong/pong_descrip.png', 193, 237, 991, 391)
        game_button2 = Button('assets/games/pong/pong_game_pic.png', 1336, 210, 520, 521)
        game_button3 = Button('assets/games/pong/return.png', 193, 795, 514, 180)
        game_button4 = Button('assets/games/pong/play.png', 1336, 795, 514, 180)
        game_button5 = Button('assets/games/pong/pong_title.png', 703, 0, 388, 138)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['pong'] = game_objects

#this function initializes the object list with things for the risk game screen
    def init_game_risk(self):
        game_objects = []
        game_bkgd = Button('assets/games/risk/risk_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/risk/risk_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/risk/risk_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/risk/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/risk/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/risk/risk_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['risk'] = game_objects

#this function initializes the object list with things for the scrabble game screen
    def init_game_scrabble(self):
        game_objects = []
        game_bkgd = Button('assets/games/scrabble/scrabble_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/scrabble/scrabble_descrip.png', 125, 216, 979, 87)
        game_button2 = Button('assets/games/scrabble/scrabble_game_pic.png', 1312, 216, 500, 500)
        game_button3 = Button('assets/games/scrabble/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/scrabble/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/scrabble/scrabble_title.png', 686, 72, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['scrabble'] = game_objects

#this function initializes the object list with things for the settlers of kataan game screen
    def init_game_settlers_katan(self):
        game_objects = []
        game_bkgd = Button('assets/games/settlers_katan/settlers_katan_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/settlers_katan/settlers_katan_descrip.png', 135, 185, 979, 87)
        game_button2 = Button('assets/games/settlers_katan/settlers_katan_game_pic.png', 1315, 174, 500, 500)
        game_button3 = Button('assets/games/settlers_katan/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/settlers_katan/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/settlers_katan/settlers_katan_title.png', 686, 72, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['settlers_katan'] = game_objects

#this function initializes the object list with things for the snakes and ladders game screen
    def init_game_snakes_ladders(self):
        game_objects = []
        game_bkgd = Button('assets/games/snakes_ladders/snakes_ladders_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/snakes_ladders/snakes_ladders_descrip.png', 193, 179, 968, 585)
        game_button2 = Button('assets/games/snakes_ladders/snakes_ladders_game_pic.png', 1336, 179, 500, 500)
        game_button3 = Button('assets/games/snakes_ladders/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/snakes_ladders/play.png', 1336, 831, 514, 180)
        game_button5 = Button('assets/games/snakes_ladders/snakes_ladders_title.png', 631, 46, 658, 59)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['snakes_ladders'] = game_objects

#this function initializes the object list with things for the sorry game screen
    def init_game_sorry(self):
        game_objects = []
        game_bkgd = Button('assets/games/sorry/sorry_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/sorry/sorry_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/sorry/sorry_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/sorry/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/sorry/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/sorry/sorry_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['sorry'] = game_objects

#this function initializes the object list with things for the taboo game screen
    def init_game_taboo(self):
        game_objects = []
        game_bkgd = Button('assets/games/taboo/taboo_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/taboo/taboo_descrip.png', 116, 172, 979, 87)
        game_button2 = Button('assets/games/taboo/taboo_game_pic.png', 1300, 172, 500, 500)
        game_button3 = Button('assets/games/taboo/return.png', 128, 730, 514, 180)
        game_button4 = Button('assets/games/taboo/play.png', 1320, 730, 514, 180)
        game_button5 = Button('assets/games/taboo/taboo_title.png', 800, 35, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['taboo'] = game_objects

#this function initializes the object list with things for the taboo game screen
    def init_game_words_with_friends(self):
        game_objects = []
        game_bkgd = Button('assets/games/words_with_friends/words_with_friends_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/words_with_friends/words_with_friends_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/words_with_friends/words_with_friends_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/words_with_friends/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/words_with_friends/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/words_with_friends/words_with_friends_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['words_with_friends'] = game_objects


#this function initializes the object list with things for the checkers game screen
    def init_game_yahtzee(self):
        game_objects = []
        game_bkgd = Button('assets/games/yahtzee/yahtzee_bkgd.png', 0, 0, 1920, 1080)
        game_button1 = Button('assets/games/yahtzee/yahtzee_descrip.png', 159, 198, 979, 87)
        game_button2 = Button('assets/games/yahtzee/yahtzee_game_pic.png', 1324, 180, 600, 600)
        game_button3 = Button('assets/games/yahtzee/return.png', 193, 831, 514, 180)
        game_button4 = Button('assets/games/yahtzee/play.png', 1324, 831, 514, 180)
        game_button5 = Button('assets/games/yahtzee/yahtzee_title.png', 570, 40, 634, 62)
        sound_button_on = Button('assets/sound_on.png', 0, 955, 125, 125)
        game_objects.append(game_bkgd)
        game_objects.append(sound_button_on)
        game_objects.append(game_button1)
        game_objects.append(game_button2)
        game_objects.append(game_button3)
        game_objects.append(game_button4)
        game_objects.append(game_button5)
        self.objects['yahtzee'] = game_objects



