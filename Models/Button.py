import pygame
from Constants.Screen import Screen

class Button(object):
    """
    Draws buttons onto the screen using objects from the initializations.objects list
    """

    def __init__(self, image_path, x, y, width, height):

        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.image = pygame.image.load(image_path)
        self.screen = pygame.display.set_mode((Screen.SCREEN_WIDTH, Screen.SCREEN_HEIGHT), pygame.NOFRAME)

    def draw(self):
        self.screen.blit(self.image, (self.x, self.y))

    #this function checks if the mouse position was within the button object when the mouse
    #was clicked
    def within_button(self, x, y):
        return self.x < x < (self.x + self.width) and self.y < y < (self.y + self.height)

