from Models.Button import Button
from EventManager import *

class GameEngine(object):
    """
    Tracks the game state.
    """

    def __init__(self, event_manager):
        """
        evManager (EventManager): Allows posting messages to the event queue.
        
        Attributes:
        running (bool): True while the engine is online. Changed via QuitEvent().
        """
        
        self.event_manager = event_manager
        event_manager.RegisterListener(self)
        self.running = False
        self.state = StateMachine()


    def notify(self, event):
        """
        Called by an event in the message queue. 
        """

        if isinstance(event, QuitEvent):
            self.running = False
        if isinstance(event, StateChangeEvent):
            # pop request
            if not event.state:
                # false if no more states are left
                if not self.state.pop():
                    self.event_manager.Post(QuitEvent())
            else:
                # push a new state on the stack
                self.state.push(event.state)

    def run(self):
        """
        Starts the game engine loop.

        This pumps a Tick event into the message queue for each loop.
        The loop ends when this object hears a QuitEvent in notify(). 
        """
        self.running = True
        self.event_manager.Post(InitializeEvent())
        self.state.push(STATE_CATEGORIES)
        while self.running:
            newTick = TickEvent()
            self.event_manager.Post(newTick)


# State machine constants for the StateMachine class below
STATE_INTRO = 1
STATE_CATEGORIES = 2
STATE_HELP = 3
STATE_ABOUT = 4
STATE_ADULT = 5
STATE_KIDS = 6
STATE_CHILL = 7
STATE_PARTY = 8
STATE_GAME_GENERIC = 9
STATE_GAME_POKER = 10
STATE_GAME_CARDS_AG_HUM = 11
STATE_GAME_CHESS = 12
STATE_GAME_SCRABBLE = 13
STATE_GAME_HEADS_UP = 14
STATE_GAME_LIFE = 15
STATE_GAME_SETTLERS_KATAN = 16
STATE_GAME_CANDY_LAND = 17
STATE_GAME_SNAKES_LADDERS = 18
STATE_GAME_CHECKERS = 19
STATE_GAME_CHINESE_CHECKERS = 20
STATE_GAME_MONOPOLY = 21
STATE_GAME_YAHTZEE = 22
STATE_GAME_MAHJONG = 23
STATE_GAME_WORDS_WITH_FRIENDS = 24
STATE_GAME_JENGA = 25
STATE_GAME_HANGMAN = 26
STATE_GAME_MANCALA = 27
STATE_GAME_RISK = 28
STATE_GAME_TABOO = 29
STATE_GAME_APPLES_TO_APPLES = 30
STATE_GAME_SORRY = 31
STATE_GAME_BATTLESHIP = 32
STATE_GAME_MAFIA = 33
STATE_GAME_PONG = 34
STATE_ADULT2 = 35
STATE_PARTY2 = 36
STATE_KIDS2 = 37
STATE_CHILL2 = 38



class StateMachine(object):
    """
    Manages a stack based state machine.
    peek(), pop() and push() perform as traditionally expected.
    peeking and popping an empty stack returns None.
    """
    
    def __init__ (self):
        self.statestack = []
    
    def peek(self):
        """
        Returns the current state without altering the stack.
        Returns None if the stack is empty.
        """
        try:
            return self.statestack[-1]
        except IndexError:
            # empty stack
            return None
    
    def pop(self):
        """
        Returns the current state and remove it from the stack.
        Returns None if the stack is empty.
        """
        try:
            self.statestack.pop()
            return len(self.statestack) > 0
        except IndexError:
            # empty stack
            return None
    
    def push(self, state):
        """
        Push a new state onto the stack.
        Returns the pushed value.
        """
        self.statestack.append(state)
        return state
