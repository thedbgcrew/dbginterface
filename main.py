import EventManager
from Models import GameEngine
from Controllers import ViewController
from Controllers import DBGIController
from Controllers.Initializers import Initializations

def run():
    event_manager = EventManager.EventManager()
    game_engine = GameEngine.GameEngine(event_manager)
    initializations = Initializations.Initializations()
    initializations.fill_object_dict()
    graphics = ViewController.ViewController(event_manager, game_engine, initializations)
    keyboard = DBGIController.DBGIController(event_manager, game_engine, initializations)


    game_engine.run()

if __name__ == '__main__':
    run()
